from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    username = models.CharField(max_length=100, db_column="username")
    email = models.EmailField(db_column="email")
    is_admin = models.BooleanField(default=False, db_column="admin")
    is_active = models.BooleanField(default=True, db_column="active")
    is_deleted = models.BooleanField(default=False, db_column="deleted")
    user = models.OneToOneField(User, on_delete=models.CASCADE, db_column="user")

    class Meta:
        db_table = "Profile"

    def __str_(self):
        return f"{self.username} {self.pk}"
