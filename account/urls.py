from django.urls import path
from rest_framework_simplejwt.views import (

    TokenRefreshView,
)

from . import views

app_name = 'account'
urlpatterns = [

    path('register/', views.userRegister, name="userRegister"),
    path('login/', views.userLogin, name="userLogin"),
    path('all-users/', views.listAllUsers, name="listAllUsers"),
    path('all-admin-users/', views.listAllAdminUsers, name="listAllAdminUsers"),
    path('all-public-users/', views.listAllPublicUsers, name="listAllPublicUsers"),
    path('me/', views.checkCurrentUser, name="checkCurrentUser"),
    path('refresh/', TokenRefreshView.as_view(), name='token_refresh')

]
