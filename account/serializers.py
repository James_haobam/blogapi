from rest_framework import serializers
from django.contrib.auth.models import User
from . models import  Profile


class AllUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        read_only_fields = ('id',)
        fields = ["id", "username", "email", "is_admin"]


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        read_only_fields = ("id",)
        # fields = ["id", "username", "email", "is_staff", "first_name", "last_name", "is_active", "date_joined"]
        fields = ["id", "username", "email", "is_admin", "is_active","user"]
