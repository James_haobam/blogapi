from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from rest_framework import status

from .models import  Profile
from .auth import get_token_for_user
from .serializers import AllUserSerializer,UserSerializer


@api_view(["POST"])
def userRegister(req):
    try:

        print(req.data)
        if not req.user.is_authenticated:

            user = User.objects.filter(username=req.data["username"]).exists()

            if (user):

                return Response({
                    "status": 409,
                    "detail": "Profile with the username already exists",

                }, status=status.HTTP_409_CONFLICT)
            else:

                user = User.objects.create_user(username=req.data["username"], password=req.data["password"],
                                                email=req.data["email"])

            return Response({
                "status": 200,
                "detail": "User created!",
                #   "data":profile.id
            }, status=status.HTTP_200_OK)
        else:

            print("Not allowed")
            return Response({
                "status": 403,
                "detail": "Logout first"
            }, status=status.HTTP_403_FORBIDDEN)


    except Exception as e:
        print("ERROR " + str(e))
        return Response({
            "status": 500,
            "detail": f"Error, {e}",

        }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
def userLogin(req):
    data = req.data
    currentuser = req.user
    try:
        print(currentuser.is_authenticated)
        if currentuser.is_authenticated:
            return Response({
                "status": 403,
                "detail": "Logout first"

            }, status=status.HTTP_403_FORBIDDEN)
        user_name = data["username"]
        user_pass = data["password"]

        user = authenticate(username=user_name, password=user_pass)
        if user is None:
            print("User with the credentials not found")

            return Response({
                "status": 404,
                "detail": "User with the given credentials not found.Check the credentials again"
            }, status=status.HTTP_404_NOT_FOUND)

        token = get_token_for_user(user)

        return Response(
            token, status=status.HTTP_200_OK
        )
    except Exception as e:
        print(e)
        return Response({
            "status": 500,
            "detail": "Error has occurred in login"
        }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def listAllUsers(req):
    try:
        users = User.objects.filter(is_active=True)
        serializers = AllUserSerializer(users, many=True)
        return Response({
            "status": 200,
            "detail": "success",
            "data": serializers.data
        }, status=status.HTTP_200_OK)
    except Exception as e:
        print(e)
        return Response({
            "status": 500,
            "detail": "failed"

        }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def listAllAdminUsers(req):
    try:
        adminUsers = Profile.objects.filter(is_admin=True,is_active=True,is_deleted=False)
        serializers = AllUserSerializer(adminUsers, many=True)
        return Response({
            "status": 200,
            "detail": "success",
            "data": serializers.data
        }, status=status.HTTP_200_OK)

    except Exception as e:
        print(e)
        return Response({
            "status": 500,
            "detail": "failed"

        }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def listAllPublicUsers(req):
    try:
        publicUsers = Profile.objects.filter(is_admin=False,is_active=True,is_deleted=False)
        serializers = AllUserSerializer(publicUsers, many=True)
        return Response({
            "status": 200,
            "detail": "success",
            "data": serializers.data
        }, status=status.HTTP_200_OK)

    except Exception as e:
        print(e)
        return Response({
            "status": 500,
            "detail": "failed"

        }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def checkCurrentUser(req):
    try:
        user = req.user
        profile =  Profile.objects.get(id = user.id)

        serializer =  UserSerializer(
            profile,
            many=False
                       )
        print(serializer)
        return  Response({
            "status":200,
            "detail":"success",
            "data":serializer.data
        },status=status.HTTP_200_OK)
    except Exception as e:
        return  Response({
            "status":500,
            "detail":f"failed {e}",

        },status=status.HTTP_500_INTERNAL_SERVER_ERROR)
