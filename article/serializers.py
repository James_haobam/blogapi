from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Article


class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        read_only_feilds = ('id',)
        fields = ["id", "title", "description", "source_link", "poster","company"]


class CustomArticleSerializer(serializers.Serializer):
    id=serializers.IntegerField()
    title = serializers.CharField(max_length=250)
    description = serializers.CharField(max_length=500)
    source_link = serializers.CharField(max_length=300)
    poster = serializers.IntegerField()
    company = serializers.IntegerField()


class PostArticleSerializer(serializers.Serializer):

    title = serializers.CharField(max_length=250)
    description = serializers.CharField(max_length=500)
    source_link = serializers.CharField(max_length=300, allow_blank=True, allow_null=True)
    is_deleted = serializers.BooleanField(default=False)
    poster = serializers.IntegerField()
    is_active = serializers.BooleanField(default=True)
    is_archived = serializers.BooleanField(default=False)
