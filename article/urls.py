from django.urls import path
from . import views

app_name = 'article'
urlpatterns = [
    path('list/', views.listArticles, name="listArticles"),
    path('add-blog/', views.addBlogData, name="addBlogData"),
    path('update-blog/', views.updateBlog, name="updateBlog"),
    path('delete-blog/', views.deleteBlog, name="deleteBlog"),
    path('detail-blog/', views.detailBlog, name="detailBlog"),
    #Company wise blog list
    path('blogs-company/', views.blogCompany, name="blogCompany"),
]
