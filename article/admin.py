from django.contrib import admin
from .models import Article ,Company


class ArticleAdmin(admin.ModelAdmin):
    readonly_fields = ("id","poster")
class CompanyAdmin(admin.ModelAdmin):
    readonly_fields = ("id",)

admin.site.register(Article,ArticleAdmin)

admin.site.register(Company,CompanyAdmin)
