from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework import status
from django.db import connection

from .models import Article, Company
from .serializers import ArticleSerializer, PostArticleSerializer
from account.models import Profile


@api_view(["GET"])
@permission_classes([IsAuthenticatedOrReadOnly])
def listArticles(req):
    try:

        articles = Article.objects.filter(is_deleted=False, is_active=True, is_archived=False)

        serializer = ArticleSerializer(articles, many=True)

        return Response({
            "status": 200,
            "detail": "success",
            "data": serializer.data
        }, status=status.HTTP_200_OK)
    except Exception as e:

        return Response({
            "status": 500,
            "detail": str(e),
        }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def addBlogData(req):
    data = req.data
    try:
        print(data)

        user = req.user
        compId = data["company"]
        print(compId)
        Company.objects.get(id=compId)

        profile = Profile.objects.get(id=user.id)

        data.update({
            "poster": profile.id

        })
        serializer = ArticleSerializer(data=data)
        # print(serializer)

        if serializer.is_valid():
            print("Valid")
            serializer.save()
            # serializer.save()

            return Response({
                "status": 201,
                "detail": "success",
                "data": serializer.data
            }, status=status.HTTP_201_CREATED)
        else:
            print(serializer.errors)
            return Response({
                "status": 400,

                "detail": "faied!, Bad data format",

            }, status=status.HTTP_400_BAD_REQUEST)

    except Company.DoesNotExist:

        return Response({
            "status": 404,
            "detail": "Company associate with the id does not found!",

        }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    except Exception as e:

        Response({
            "status": 500,
            "detail": str(e),

        }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def addBlog(req):
    data = req.data
    try:
        print(data)

        user = req.user
        data.update({
            "poster": user.id
        })
        serializer = PostArticleSerializer(data=data)
        # print(serializer)

        if serializer.is_valid():
            print("Valid")
            title = serializer.validated_data["title"]
            description = serializer.validated_data["description"]
            source_link = serializer.validated_data["source_link"]
            is_deleted = serializer.validated_data["is_deleted"]
            poster = serializer.validated_data["poster"]
            is_active = serializer.validated_data["is_active"]
            is_archived = serializer.validated_data["is_archived"]
            # serializer.save()

            print(title, description, source_link, is_deleted, poster, is_active, is_archived)
            with connection.cursor() as cursor:
                sql_query = """
                    INSERT INTO article_article (heading, content, link, deleted, poster_id, active, archived)
                    VALUES (%s, %s, %s, %s, %s, %s, %s)
                """
                cursor.execute(sql_query, (title, description, source_link, is_deleted, poster, is_active, is_archived))
                connection.commit()
            return Response({
                "status": 201,
                "detail": "success",
                "data": serializer.data
            }, status=status.HTTP_201_CREATED)
        else:
            print(serializer.errors)
            return Response({
                "status": 400,

                "detail": "faied!, Bad data format",

            }, status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:

        Response({
            "status": 500,
            "detail": str(e),

        }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["PUT"])
@permission_classes([IsAuthenticated])
def updateBlog(req):
    try:
        blog_id = req.query_params.get("id")
        user = req.user
        data = req.data
        profile = Profile.objects.get(id=user.id)
        article = Article.objects.get(id=blog_id, is_deleted=False, is_active=True, is_archived=False)

        if user.id == article.poster.id:
            print("Allowed")
            article.is_active = False
            article.is_archived = True
            article.save()

            data.update({
                "poster": profile.id
            })
            serializer = ArticleSerializer(data=data)

            if serializer.is_valid():
                serializer.save()
                return Response({
                    "status": 200,
                    "detail": "success", "data": serializer.data}, status=status.HTTP_200_OK)
            else:
                print("Invalid")
                return Response({
                    "status": 400,
                    "detail": "success", "data": serializer.data}, status=status.HTTP_400_BAD_REQUEST)
        else:
            print("No allowed")
            return Response({
                "status": 401,
                "detail": "Not allowed"}, status=status.HTTP_401_UNAUTHORIZED)


    except Article.DoesNotExist:
        return Response({
            "status": 404,
            "detail": f"Not found"}, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        print(e)
        return Response({
            "status": 500,
            "detail": f"failed!, {e}"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["DELETE"])
@permission_classes([IsAuthenticated])
def deleteBlog(req):
    try:
        blog_id = req.query_params.get("id")
        user = req.user
        article = Article.objects.get(id=blog_id, is_deleted=False)

        if user.id == article.poster.id:
            article.is_deleted = True
            article.is_archived = True
            article.is_active = False
            article.save()
            return Response({
                "status": 200,
                "detail": "success"}, status=status.HTTP_200_OK)
        else:
            print("No allowed")
            return Response({
                "status": 401,
                "detail": "Not allowed"}, status=status.HTTP_401_UNAUTHORIZED)

    except Article.DoesNotExist:
        return Response({
            "status": 404,
            "detail": f"Not found"}, status=status.HTTP_404_NOT_FOUND)

    except Exception as e:
        print(e)
        return Response({
            "status": 500,
            "detail": f"failed!, {e}"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def detailBlog(req):
    try:
        blog_id = req.query_params.get("id")

        article = Article.objects.get(id=blog_id, is_deleted=False)
        serializer = ArticleSerializer(article, many=False)

        return Response({
            "status": 200,
            "detail": "success", "data": serializer.data}, status=status.HTTP_200_OK)
    except Article.DoesNotExist:

        return Response({
            "status": 404,
            "detail": "Not found"
        }, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:

        return Response({
            "status": 500,
            "detail": f"Error, {e}"
        }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def blogCompany(req):
    try:

        companyId = req.META.get(
            'HTTP_COMP_ID')  # HTTP added and all the letters must be capital, hyphen must be underscore
        Company.objects.get(id=companyId)
        articles = Article.objects.filter(company__id=companyId, is_deleted=False, is_active=True, is_archived=False)
        serializer = ArticleSerializer(articles, many=True)
        return Response({
            "status": 200,
            "detail": serializer.data
        }, status=status.HTTP_200_OK)
    except Company.DoesNotExist:
        return Response({
            "status": 404,
            "detail": "Company does not found"

        }, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        return Response({
            "status": 500,
            "detail": str(e)

        }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
