from django.db import models
from django.contrib.auth.models import User

from account.models import  Profile


class Company(models.Model):
    name = models.CharField(max_length=250, db_column="company_name")
    description = models.TextField(max_length=500, db_column="description")

    class Meta:
        verbose_name = "Company"
        verbose_name_plural = "Companies"
        db_table = "Company"

    def __str__(self):
        return f"{self.id} {self.name}"


class Article(models.Model):
    title = models.CharField(max_length=250, db_column="heading")
    description = models.CharField(max_length=500, db_column="content")
    source_link = models.CharField(max_length=300, null=True, blank=True, db_column="link")
    is_deleted = models.BooleanField(default=False, db_column="deleted")
    poster = models.ForeignKey(Profile, on_delete=models.CASCADE, db_column="poster")
    is_active = models.BooleanField(default=True, db_column="active")
    is_archived = models.BooleanField(default=False, db_column="archived")
    company = models.ForeignKey(Company, on_delete=models.CASCADE, db_column="company", related_name="articles")

    class Meta:
        verbose_name = "Article"
        verbose_name_plural = "Articles"
        db_table = "Article"  # Rename the table manually

    def __str__(self):
        return f"{self.id} {self.title} {self.is_active}"
